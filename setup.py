from setuptools import setup


with open('requirements.txt') as f:
    required = f.read().splitlines()


setup(
    name='core',
    zip_safe=True,
    version='1.0.0',
    description='Core for analytical reports',
    url='https://gitlab.com/AlexanderJariuc/analyticalreportscore',
    maintainer='Alexandr Jariuc',
    maintainer_email='alexander.zharyuk@gmail.com',
    packages=['core'],
    install_requires=required,
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
    ],
    python_requires='>=3.8',
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
)
