import sys
import os
import configparser

if os.path.basename(sys.argv[0]) in ('durin_cli.py', 'durin_web.py'):
    from app import config
    from app import log_manager
else:
    config = configparser.ConfigParser()
    config.read(f'{os.getcwd()}/config.ini')
    from .logger import log_manager

from . import mod_sql
from . import mod_gspread
from . import mod_services
from . import logger

# for refactoring
from .report_base import ReportBase
from .mod_logger import log_report
from .mod_profiler import profile_report
from .build_base import BuildBase
