import sys
import pandas as pd
import numpy as np
import inspect
import re
import datetime

from . import ReportBase
from . import log_report


PYTHON_TO_PANDAS_MAP = {
    'int': 'Int64',
    'float': 'float64',
    'str': 'object',
    'date': np.datetime64,
    'datetime': np.datetime64
}


class BuildBase(ReportBase):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.connector_name = ''
        self.build_name = ''
        self.essential_fields = {}

    def run(self):
        # Загружаем коннектор
        connector_build = self.download_connector()
        # Нормализуем колонки: приводим типы, устанавливаем порядок, добавляем недостоющие поля
        build = self.normalize_columns(connector_build, self.essential_fields)
        # Проверка валидности
        self.data_quality_check(build)
        # Выгружаем билд
        self.upload_build(build)

    def download_connector(self):
        table_name = self.connector_name
        connector = self.client_db.download_table(table_name)

        return connector

    def upload_build(self, build):
        table_name = self.build_name
        self.client_db.upload_table(table_name, build)

    def data_quality_check(self, build):
        pass

    def normalize_columns(self, df, essential_fields):
        df = self.existence_control(df, essential_fields)
        df = self.order_control(df, essential_fields)
        df = self.type_control(df, essential_fields)

        return df

    @staticmethod
    def existence_control(df: pd.DataFrame, essential_fields: dict):
        for column in essential_fields.keys():
            if column not in df.columns:
                df[column] = None

        return df

    @staticmethod
    def order_control(df: pd.DataFrame, essential_fields: dict):
        essential_columns = list(essential_fields.keys())
        extra_columns = [column for column in df.columns if column not in essential_columns]
        order = essential_columns + extra_columns
        df = df[order]

        return df

    @staticmethod
    def type_control(df: pd.DataFrame, essential_fields: dict):
        for column, dtype in essential_fields.items():
            # Запоминаем, где были пропуски, чтобы потом вернуть их
            null_values = df[column].isna()

            # Числа нормализуем, убираем все лишние символы
            if dtype in (int, float):
                df[column] = df[column].astype(str).apply(lambda x: re.sub(r"[^0-9\.]+", "", x)).replace('', '0')

            # Приводим поле к правильному типу
            pandas_dtype = PYTHON_TO_PANDAS_MAP.get(dtype, 'object')
            if dtype == 'date':
                df[column] = pd.to_datetime(df[column], errors='coerce').dt.normalize()
            else:
                df[column] = df[column].astype(pandas_dtype, errors='ignore').mask(null_values, np.nan)
        return df

    def dq_not_null(self, df: pd.DataFrame, fields: list):
        for field in fields:
            amount_null = int(df[field].isnull().sum())

            if amount_null:
                self.warnings[f'Null values ({field})'] = amount_null

    def dq_duplicates(self, df: pd.DataFrame, fields: list):
        amount_duplicates = len(df[df.duplicated(fields)])

        if amount_duplicates:
            self.warnings[f"Duplicates ({' '.join(fields)})"] = amount_duplicates

    def dq_consistency_by_date(self, df: pd.DataFrame, field: str, delay: int):
        last_date = datetime.datetime.now().date() - datetime.timedelta(days=delay)
        amount_records = len(df[df[field] == str(last_date)])

        if amount_records == 0:
            self.warnings[f'Not consistency by date ({field}: {str(last_date)})'] = amount_records



