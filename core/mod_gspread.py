import pandas as pd
import gspread

def download_gspread(sheet_key=None, worksheet_id=0, cell_range=None,
                    columns_range=None, row_range=None):
    """Функция для подключения к спредшиту.
        
        Токен для сервисного аккаунта добавляется в 
        %APPDATA%\gspread\service_account.json
        
        sheet_key (str) - уникальный идентификатор из url.
        worksheet_id (int, str) - либо номер, либо название 
            листа в таблице. Первая страница всегда имеет номер 0.
        cell_range (srt) - фиксированный диапазон для скачивания "A1:B1".
        columns_range (str) - диапазон столбцов для скачивания "1:7".
        row_range (str) - диапазон строк для скачивания "1:7".
        """
    gc = gspread.service_account()
    sht = gc.open_by_key(sheet_key)
    if type(worksheet_id) != str:
        worksheet = sht.get_worksheet(worksheet_id)
    else:
        worksheet = sht.worksheet(worksheet_id)
    
    if cell_range:
        data = worksheet.get(cell_range) 
        columns = data.pop(0)
        df = pd.DataFrame(data, columns=columns)
    elif columns_range:  
        data = {}
        column_1 = int(columns_range.split(':')[0])
        column_2 = int(columns_range.split(':')[1])
        # Ограничиваем range в спредшите 
        for i in range(column_1, column_2):
            values = worksheet.col_values(i)
            data[values.pop(0)] = values
        # Обрабатываем пропуски в колонках, если они в самом конце
        # Добавляем к значениям None
        for column, arr in data.items():
            length_dif = len(max(data.values())) - len(arr) 
            if length_dif:
                data[column].extend([None]*length_dif)
        df = pd.DataFrame(data)
    elif row_range:
        data = []
        row_1 = int(row_range.split(':')[0])
        row_2 = int(row_range.split(':')[1])
        # Ограничиваем range в спредшите 
        for i in range(row_1, row_2):
            values = worksheet.row_values(i)
            data.append(values)
        print(data)
        columns = data.pop(0)
        df = pd.DataFrame(data, columns=columns)
    # Забираем все, что есть в документе
    else:
        data = worksheet.get_all_records()
        df = pd.DataFrame(data)
    return df

def update_gspread(df, sheet_key=None, worksheet_id=0, initial_cell='A1'):
    """Функция для обновления (update) листа (worksheet) существующего спредшита.
        Спредшит и воркшит должны существовать.

        Токен для сервисного аккаунта добавляется в 
        %APPDATA%\gspread\service_account.json
        
        df (pandas.DataFrame) - датафрейм для загрузки в GS
        sheet_key (str) - уникальный идентификатор из url.
        worksheet_id (int, str) - либо номер, либо название 
            листа в таблице. Первая страница всегда имеет номер 0.
        initial_cell (str) - Индекс ячейки, с которой начнется вставка
        """
    gc = gspread.service_account()
    sht = gc.open_by_key(sheet_key)
    if type(worksheet_id) != str:
        worksheet = sht.get_worksheet(worksheet_id)
    else:
        worksheet = sht.worksheet(worksheet_id)
    # Чистим страницу перед записью
    worksheet.clear()
    # Загружаем датафрейм в указанный spreadtshhet-worksheet.
    # ВАЖНО: value_input_option='USER_ENTERED' - позволяет загрузить данные в ячейку как есть, 
    #   без подстановки в ячейку апострофа (апостроф \' превращает значение ячейки в формат 'текст')
    worksheet.update(initial_cell, [df.columns.values.tolist()] + df.values.tolist(), value_input_option='USER_ENTERED')
