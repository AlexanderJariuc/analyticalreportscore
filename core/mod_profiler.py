import cProfile
import pstats
import tracemalloc


class Profiler:
    def __init__(self):
        self.stats = []

    def print_stats(self):
        result_line = ''

        run_time = self.stats[-1][1]
        max_memory = max([el[3] for el in self.stats])

        del self.stats[-1]

        result_line += f'\nВремя выполнения: {run_time:.3f} с\n'
        result_line += f'Максимальный размер потребляемой памяти: {max_memory/2**20:.3f} MB\n\n'

        result_line += f'{"Название":45}|{" Время выполнения":30}  |{" Текущая память":30}   |{" Пиковая память":30}\n'
        for stat in self.stats:
            result_line += f'{stat[0]:45}|{stat[1]:30.5f} c|{stat[2]/2**20:30.5f} MB|{stat[3]/2**20:30.5f} MB\n'

        print(result_line)

    def profile_func(self, func):

        def wrapper(*args, **kwargs):
            profiler = cProfile.Profile()
            result = profiler.runcall(func, *args, **kwargs)

            # get time stats
            profiler.dump_stats('profile_log')
            ps = pstats.Stats('profile_log').strip_dirs()
            try:
                _, execution_time = [(key[2], value[3]) for key, value in ps.stats.items() if func.__name__ in key[2]][0]
            except IndexError:
                execution_time = '-'

            # get memory stats
            current_size, current_peak = tracemalloc.get_traced_memory()

            # get union stats
            self.stats.append((func.__name__, execution_time, current_size, current_peak))

            if func.__name__ == 'run':
                self.print_stats()

            return result

        return wrapper

    def profile_batch_download(self, func):

        def wrapper(*args, **kwargs):
            profiler = cProfile.Profile()
            for batch, processed_row, total_rows in profiler.runcall(func, *args, **kwargs):

                # get time stats
                profiler.dump_stats('profile_log')
                ps = pstats.Stats('profile_log').strip_dirs()

                execution_time = 0

                # get memory stats
                current_size, current_peak = tracemalloc.get_traced_memory()

                # get union stats
                self.stats.append((func.__name__, execution_time, current_size, current_peak))

                yield batch, processed_row, total_rows

        return wrapper


def profile_report(cls):
    profiler = Profiler()
    tracemalloc.start()
    for attr in [attr for attr in cls.__dict__.keys() if not attr.startswith('_')]:
        if callable(getattr(cls, attr)):
            if attr.startswith(('run', 'download', 'upload', 'build', 'create_view', 'processing')):
                setattr(cls, attr, profiler.profile_func(getattr(cls, attr)))
            elif attr.startswith('batch_download'):
                setattr(cls, attr, profiler.profile_batch_download(getattr(cls, attr)))
    return cls