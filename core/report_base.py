import os
import sys
import configparser
from abc import ABCMeta, abstractmethod

from . import log_manager
from . import config
from .mod_sql import PostgreManager


class ReportBase(metaclass=ABCMeta):

    def __init__(self, client=None, report=None):

        # main
        self.config = config
        self.log_manager = log_manager
        self.report = report
        self.creds = self.get_creds(client)
        self.warnings = {}
        self.client = client
        # DB
        self.client_db = PostgreManager(self.creds)
        self.service_db = PostgreManager(self.get_creds('xo_db'))
        # service frags
        self.overwrite_table = True


    @abstractmethod
    def run(self):
        pass

    def get_creds(self, client):
        host = self.config['postgresql']['host']
        port = self.config['postgresql']['port']
        user = self.config['postgresql']['user']
        password = self.config['postgresql']['password']
        database = client

        return f"dbname={database} user={user} password={password} host={host} port={port}"

    def create_function_create_extract(self):
        function_name = 'create_extract'
        self.client_db.execute_query(
            f"""
                DROP PROCEDURE if exists {function_name} CASCADE;
                CREATE OR REPLACE PROCEDURE {function_name}(id text, lake_table text, extract_table text) AS $$
                    BEGIN
                        EXECUTE ' drop table if exists ' || extract_table || ' CASCADE ';
                        EXECUTE ' create table ' || extract_table || ' as'
                                '   (with current_version as ('
                                '       select distinct on(' || id || ', int_miner_name) ' || id || ', int_miner_name, int_creation_date'
                                '       from ' || lake_table || ''
                                '       order by ' || id || ',int_miner_name, int_creation_date desc)'
                                ' select *'
                                ' from ' || lake_table || ''
                                ' join current_version using (' || id || ', int_miner_name, int_creation_date))';
                    END;
                $$ LANGUAGE plpgsql;
            """)

    def create_function_create_dependent_extract(self):
        function_name = 'create_dependent_extract'
        self.client_db.execute_query(
            f"""
                DROP PROCEDURE if exists {function_name} CASCADE;
                CREATE OR REPLACE PROCEDURE {function_name}(id_in_main text, main_lake text, id_in_sub text, sub_lake text, extract_table text) AS $$
                    BEGIN
                        EXECUTE ' drop table if exists ' || extract_table || ' CASCADE ';
                        EXECUTE ' create table ' || extract_table || ' as'
                                '   (with current_version as ('
                                '       select distinct on(' || id_in_main || ', int_miner_name) ' || id_in_main || ' as ' || id_in_sub || ', int_miner_name, int_creation_date'
                                '       from ' || main_lake || ''
                                '       order by ' || id_in_sub || ',int_miner_name, int_creation_date desc)'
                                ' select *'
                                ' from ' || sub_lake || ''
                                ' join current_version using (' || id_in_sub || ', int_miner_name, int_creation_date))';
                    END;
                $$ LANGUAGE plpgsql;
            """)

    def create_function_create_completely_updated_extract(self):
        function_name = 'create_completely_updated_extract'
        self.client_db.execute_query(
            f"""
                DROP PROCEDURE if exists {function_name} CASCADE;
                CREATE OR REPLACE PROCEDURE {function_name}(lake_table text, extract_table text) AS $$
                    BEGIN
                        EXECUTE ' drop table if exists ' || extract_table || ' CASCADE ';
                        EXECUTE ' create table ' || extract_table || ' as'
                                '   (with current_version as ('
                                '       select distinct on(int_miner_name) int_miner_name, int_creation_date'
                                '       from ' || lake_table || ''
                                '       order by int_miner_name, int_creation_date desc)'
                                ' select *'
                                ' from ' || lake_table || ''
                                ' join current_version using (int_miner_name, int_creation_date))';
                    END;
                $$ LANGUAGE plpgsql;
            """)

    def create_function_create_extract_one_for_day(self):
        function_name = 'create_extract_one_for_day'
        # такая же как create_extract только еще берет максимальный id за день (id строки из майнера, а не передаваемый в параметре id текст)
        self.client_db.execute_query(
            f"""
                DROP PROCEDURE if exists {function_name} CASCADE;
                CREATE OR REPLACE PROCEDURE {function_name}(id text, lake_table text, extract_table text) AS $$
                    BEGIN
                        EXECUTE ' drop table if exists ' || extract_table || ' CASCADE ';
                        EXECUTE ' create table ' || extract_table || ' as'
                                '   (with current_version as ('
                                '       select distinct on(' || id || ', int_miner_name) ' || id || ', int_miner_name, int_creation_date, id'
                                '       from ' || lake_table || ''
                                '       order by ' || id || ',int_miner_name, int_creation_date desc, id desc)'
                                ' select *'
                                ' from ' || lake_table || ''
                                ' join current_version using (' || id || ', int_miner_name, int_creation_date, id))';
                    END;
                $$ LANGUAGE plpgsql;
            """)
