import pandas as pd
import calendar
from .mod_sql import download_table
import traceback

def build_plan(df_plan, weigths_in_metrics, df_weights=pd.DataFrame(), rename_plan=True):
    """Функция для создания датафрэйма с планами.
    На вход подаются: 
    df_paln - датафрэйм с данными из спредшита по планам на конкретную сущность.
                в спредшите должны быть колонки: d_month | (dimensions) | (metrics)
                Формат d_month - 'mm-YYYY'
    weigths_in_metrics (dict) - словарь, где ключи это название метрик в спредшите,
                а значение - тип распределения плана по месяцу. 
                Возможны следующие типы распределения: 
                - m_weight - распределение по дням месяца происходит в зависимости от 
                    таблицы с весами, которую передали в функцию
                - m_equal_weight - распределение происходит равномерно по всем дням месяца
                - m_no_weight - распределение по дням отсутствует. Вся метрика приходится на 
                    первое число месяца для которого она проставлена.
    df_weights - датафрэйм с весами для каждого дня. Две колонки - day и weight. Причем вес для
                каждого дня считается как отношение веса для этого дня на сумму весов всех дней 
                в в этом месяце. Так сделано из-за того, что в разных месяцах разное колич. дней.
                Пример такого спредшита тут:
    https://docs.google.com/spreadsheets/d/1eDo_4MZQRDCC4Tl9yAFJCAgVB7pVECRhcSJH-jpQ8bg/edit#gid=371330226

    На выходе из функции имеем датафрайм с колонками: created_at | (dimensions) | (metrics)
    К каждой метрике добавляется окончание _target."""
    
    metrics = list(weigths_in_metrics.keys())
    if not df_weights.empty:    
        df_weights.weight = df_weights.weight.astype(float)
    
    def calculate_weight(x):
        """Функция для расчета веса в зависимости от дня."""
        month_length = calendar.monthrange(x.year, x.month)[1]
        day_weight = df_weights.iloc[x.day-1].weight
        month_weight = df_weights.iloc[0:month_length].weight.sum()
        return day_weight/month_weight
    
    def get_equal_weight(x):
        month_length = calendar.monthrange(x.year, x.month)[1]
        return 1/month_length
    
    def generate_date_range(x):
        month = int(x.split('-')[0])
        year = int(x.split('-')[1])
        month_length = calendar.monthrange(year, month)[1]
        dt_range = pd.date_range(start=f'{year}-{month}-01', 
                      end=f'{year}-{month}-{month_length}')
        return dt_range.tolist()
    
    df_plan['created_at'] = df_plan.d_month.apply(generate_date_range)
    df_plan = df_plan.explode('created_at')
    if not df_weights.empty:
        df_plan['m_weight'] = df_plan.created_at.apply(calculate_weight)
    df_plan['m_equal_weight'] = df_plan.created_at.apply(get_equal_weight)
    df_plan['m_no_weight'] = df_plan.created_at.apply(lambda x: 1 if x.day == 1 else 0)
    
    df_plan['created_at'] = df_plan['created_at'].astype(str)
    
    for metric, weight_type in weigths_in_metrics.items():
        
        df_plan[metric] = df_plan[metric].str.replace(',', '')
        df_plan[metric] = df_plan[metric].replace('', '0')
        df_plan[metric] = df_plan[metric].astype(float)
        
        if rename_plan:
            df_plan[f'{metric}_target'] = df_plan[metric] * df_plan[weight_type]
            df_plan.drop(f'{metric}', axis=1, inplace=True)
        else:
            df_plan[f'{metric}'] = df_plan[metric] * df_plan[weight_type]
    
    df_plan.drop(['d_month', 'm_weight', 
                  'm_equal_weight', 'm_no_weight'], axis=1, inplace=True)
    
    return df_plan

def transfer_goods_to_targets(df_cash, df_groups):
    """Функция для определения целевой группы для номенклатуры.

        df_groups - датафрейм с названием целевой группы и названием номенклатур по уровням
        df_cash - датафрейм, где для номенклатур определяется целевая аудитория (d_target_type)"""

    df_result = pd.DataFrame()

    for target_name, *goods_names in df_groups.values:
        for i, group_name in enumerate(goods_names):
            df = df_cash[df_cash[f'd_good_group_lvl{i + 1}'] == group_name]
            df['d_target_type'] = target_name
            df_result = df_result.append(df)
    return  df_result

def validate_table_data(table_to_check, creds, columns_for_nullcheck=[], links_dict={}):
    """Функция для валидации данных в определенных полях

        table_to_check (str) - таблица, для которой делается проверка
        creds (str) - доступ к базе;
        columns_to_check (list) - список полей для проверки на пропуски;
        links_dict (dict) - словарь, где ключm это полt для проверки (str),
        а значения (tuple) - таблица, на которые должна вести ссылка из поля и название поля в исходной таблице.

        Пример словаря: {'l_purchase_id': ('extract_1c_items_in_cash', 'l_transaction_id')}
        """

    # ищем нулевые значения
    df_nulls = pd.DataFrame(columns=list(table_to_check.columns) + ['validation_problem_null'])

    # Проходим в цикле по всем полям и отбираем строки, в которых эти поля пустые
    for column in columns_for_nullcheck:
        df_temp = table_to_check[table_to_check[column].isna()].copy()
        df_temp['validation_problem_null'] = column
        df_nulls = df_nulls.append(df_temp)

    if len(df_nulls) > 0:
        df_nulls = df_nulls.groupby(list(table_to_check.columns), dropna=False).agg(
            {'validation_problem_null': lambda x: ', '.join(x)}).reset_index()
        # Поля с пропусками имеют тип данных object после создания. Необходимо привести их
        # правильному типу, чтобы потом смерджить
        df_nulls = df_nulls.astype({x: y.name for x, y in table_to_check.dtypes.items()})

    # ищем отсутствующие линки
    df_no_links = pd.DataFrame(columns=list(table_to_check.columns)+['validation_problem_link'])

    for check_column, (extract_table, extract_column) in links_dict.items():
        # достаем табличку, в которой лежат ссылки
        df_extract = download_table(extract_table, creds=creds)
        # Переименовываем поле, чтобы удобнее мерджить
        df_extract.rename(columns={extract_column:check_column}, inplace=True)
        # если поле проверялось на нулл и там оказался пропуск, то его не нужно учитывать при проверке ссылок
        df_temp = table_to_check[~table_to_check[check_column].isna()].copy()
        # проверяем линки
        no_links = df_temp.merge(df_extract[[check_column]],
                                           on=check_column, how='left', indicator=True)
        no_links = no_links[no_links['_merge']=='left_only'].drop('_merge', axis=1).copy()
        no_links['validation_problem_link'] = check_column

        df_no_links = df_no_links.append(no_links)

    if len(df_no_links) > 0:
        df_no_links = df_no_links.groupby(list(table_to_check.columns), dropna=False).agg(
            {'validation_problem_link': lambda x: ', '.join(x)}).reset_index()

    # собираем единый датафрейм с битыми данными
    if df_nulls.shape[0] == 0 or df_no_links.shape[0] == 0:
        df_result = df_nulls.append(df_no_links)
    else:
        df_result = df_nulls.merge(df_no_links, on=list(table_to_check.columns), how='outer')

    order = list(table_to_check.columns) + ['validation_problem_null', 'validation_problem_link']

    df_result = df_result[order] 

    return df_result

def get_branch_name(x):
    try:
        if 'unidragon.eu' in x:
            return 'Unidragon EU'
        elif 'unidragon.com' in x:
            return 'Unidragon US'
        elif 'unidragon.jp' in x:
            return 'Unidragon JP'
        else:
            return 'Others'
    except TypeError:
        return 'Others'