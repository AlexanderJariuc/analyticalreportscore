import math
import pandas as pd
from contextlib import closing
import psycopg2
import psycopg2.extras as extras
from typing import Union
import numpy as np

# pg_catalog.pg_type
SQL_TO_PD = {
    16: 'bool',
    20: 'Int64', 21: 'Int64', 23: 'Int64',
    25: 'str',
    700: 'float64', 701: 'float64', 1700: 'float64',
    1082: 'date', 1184: 'date', 1114: 'date',
}

PD_TO_SQL = {'int64': 'bigint',
             'float32': 'real',
             'int32': 'integer',
             'float64': 'double precision',
             'object': 'text',
             'datetime64[ns]': 'date',
             'bool': 'bool'}


class PostgreManager:

    def __init__(self, credentials: str):
        self.creds = credentials

    def execute_query(self, query: str):
        with closing(psycopg2.connect(self.creds)) as conn:
            with conn.cursor() as cursor:
                cursor.execute(query)
                conn.commit()

    def execute_values(self, query: str, tuples):
        with closing(psycopg2.connect(self.creds)) as conn:
            with conn.cursor() as cursor:
                extras.execute_values(cursor, query, tuples, page_size=10000)
                conn.commit()

    def find_one(self, query: str):
        with closing(psycopg2.connect(self.creds)) as conn:
            with conn.cursor() as cursor:
                cursor.execute(query)
                return cursor.fetchone()[0]

    def find_many(self, query: str, columns_description=False):
        with closing(psycopg2.connect(self.creds)) as conn:

            with conn.cursor() as cursor:
                cursor.execute(query)
                if columns_description:
                    return cursor.fetchall(), {desc[0]: desc[1] for desc in cursor.description}
                else:
                    return cursor.fetchall()

    def direct_download(self, query: str, date_with_time: bool = False) -> pd.DataFrame:
        data, columns_description = self.find_many(query, columns_description=True)
        df = self.convert_from_list_to_df(data, columns=columns_description, date_with_time=date_with_time)
        return df

    def download_table(self, table_name: str, columns_list: list = None,
                       filters: list = None, order_by: list = None,
                       limit: int = None, offset: int = None,
                       date_with_time: bool = False, may_not_exist: bool = False) -> pd.DataFrame:
        # формируем запрос
        query = (
            f"SELECT {', '.join(columns_list) if columns_list else '*'} "
            f"FROM {table_name} "
            f"{'WHERE ' + ' AND '.join(filters) if filters else ''} "
            f"{'ORDER BY ' + ', '.join(order_by) if order_by else ''} "
            f"{'LIMIT ' + str(limit) if limit else ''} "
            f"{'OFFSET ' + str(offset) if offset else ''} "
        )

        try:
            df = self.direct_download(query, date_with_time)
        except psycopg2.errors.UndefinedTable:
            if may_not_exist:
                df = pd.DataFrame()
            else:
                raise
        return df

    def batch_download_table(self, table_name: str, columns_list: list = None,
                             filters: list = None, integrity_column: str = None,
                             batch_size: int = 500000, date_with_time: bool = False) -> pd.DataFrame:

        total_rows = self.find_one(f"SELECT COUNT(*) FROM {table_name} "
                                   f"{'WHERE ' + ' AND '.join(filters) if filters else ''}")
        downloaded_rows = 0
        limit_value = None

        while downloaded_rows != total_rows:

            # формируем запрос и загружаем батч
            sql_query = (
                f"SELECT * "
                f"FROM ("
                f"  SELECT {', '.join(columns_list) if columns_list else '*'} "
                f"  FROM {table_name} "
                f"  {'WHERE ' + ' AND '.join(filters) if filters else ''}) as direct "
                f"{'WHERE ' + f'{integrity_column} >= {self.prepare_value(limit_value)}' if limit_value else ''} "
                f"ORDER BY {integrity_column} "
                f"LIMIT {batch_size}"
            )
            df = self.direct_download(sql_query, date_with_time)

            # находим последнее значение столбца целостности
            limit_value = [value for key, value in df.iloc[-1].to_dict().items() if key == integrity_column][0]

            # отфильтровываем записи с последним значением, кроме последнего батча
            if len(df) == batch_size:
                df.query(f"{integrity_column}!={self.prepare_value(limit_value)}", inplace=True)

            # увеличиваем счётчик загруженных записей
            downloaded_rows += len(df)

            yield df, downloaded_rows, total_rows


    def prepare_table(self, table_name: str, df: pd.DataFrame, datetime_with_time: bool = False):
        columns = []
        for name, dtype in df.dtypes.to_dict().items():

            if datetime_with_time and (str(dtype) == 'datetime64[ns]'):
                dtype = 'timestamp without time zone'
            else:
                dtype = PD_TO_SQL.get(str(dtype))

            column = f'{name} {dtype}'
            columns.append(column)

        self.execute_query(f"DROP TABLE IF EXISTS {table_name} CASCADE")
        self.execute_query(f"CREATE TABLE {table_name}({', '.join(columns)})")

    def upload_table(self, table_name: str, df: pd.DataFrame, clear: bool = True, datetime_with_time: bool = False):
        if clear:
            self.prepare_table(table_name, df, datetime_with_time)

        template = f"INSERT INTO {table_name} VALUES %s"
        tuples = self.convert_from_df_to_list(df)

        self.execute_values(template, tuples)

    @staticmethod
    def convert_from_list_to_df(data: list, columns: Union[list, dict], date_with_time=False) -> pd.DataFrame:
        if isinstance(columns, list):
            df = pd.DataFrame(data, columns=columns)

        elif isinstance(columns, dict):
            column_names = columns.keys()
            df = pd.DataFrame(data, columns=column_names)

            for column_name in column_names:
                column_type = SQL_TO_PD[columns[column_name]]
                if column_type != 'date':
                    df[column_name] = df[column_name].where(df[column_name].isnull(),
                                                            df[column_name].astype(column_type, errors='ignore'))
                else:
                    if not date_with_time:
                        df[column_name] = pd.to_datetime(df[column_name], errors='coerce').dt.normalize()
                    else:
                        df[column_name] = pd.to_datetime(df[column_name], errors='coerce')
        else:
            raise TypeError

        return df

    @staticmethod
    def convert_from_df_to_list(df: pd.DataFrame) -> list:
        df = df.replace({np.nan: None})
        tuples = [tuple(x) for x in df.values.tolist()]

        return tuples

    @staticmethod
    def prepare_value(value):
        if isinstance(value, (int, float, bool)):
            return value
        else:
            return f"'{value}'"


########################################################################################################################


creds = None
pd_to_sql_mapping = {'int64': 'bigint',
                     'float32': 'real',
                     'int32': 'integer',
                     'float64': 'double precision',
                     'object': 'text',
                     'datetime64[ns]': 'date',
                     'bool': 'bool'}

sql_to_pd_mapping = {'bigint': 'int64',
                     'numeric': 'float64',
                     'real': 'float32',
                     'integer': 'int32',
                     'double precision': 'float64',
                     'text': 'str',
                     'date': 'date',
                     'timestamp without time zone': 'date',
                     'bool': 'bool',
                     'boolean': 'bool'}


def download_table(table_name='empty_table', creds=creds, wide=True,
                   columns_list=['*'], date_limit=None, inp_query=None, date_with_time=False):
    """Функция для скачивания широких и длинных таблиц
        inp_query (str) - Строка запроса, имеет приоритет перед 
            остальными способами загрузки.
        table_name (str) - название таблицы, которую хотим скачать. 
            Можно использовать для широких и длинных таблиц.
        creds (str) - крэды в виде строки для доступа к БД:
            'dbname={} user={} password={} host={}'
        wide (bool) - флаг для указания, что таблица широкая, без
            него подефолту длинная.
        columns_list (list) - список, в котором поля для скачивания.
            Подефолту выгружаются все столбцы. 
        date_limit (dict) - словарь, ограничение по датам.
            Ключ это название колонки, а значение это дата от 
            которой выгружать даные."""

    if not inp_query:
        if wide:

            query = f"SELECT {', '.join(columns_list)} FROM {table_name}"

            if date_limit:
                query += f" WHERE {list(date_limit.keys())[0]} >= '{list(date_limit.values())[0]}'"
        else:
            query = f"SELECT * FROM {table_name}"

            if columns_list != ['*']:
                columns_names = list(map(lambda x: "'" + x + "'", columns_list))
                query += f" WHERE key in ({', '.join(columns_names)})"
    else:
        query = inp_query

    with closing(psycopg2.connect(creds)) as conn:
        with conn.cursor() as cursor:
            cursor.execute(query)
            column_names = [desc[0] for desc in cursor.description]
            data = cursor.fetchall()
            df = pd.DataFrame(data, columns=column_names)

            if not inp_query:

                cursor.execute(f"""select column_name, data_type from information_schema.columns
                               where table_name = {"'" + table_name.split('.')[-1] + "'"}
                                     and table_schema = {"'" + (table_name.split('.')[0] if len(table_name.split('.')) > 1 else 'public') + "'"}""")
                column_types = dict(cursor.fetchall())

                for column_name in column_names:
                    column_type = sql_to_pd_mapping[column_types[column_name]]
                    if column_type != 'date':
                        df[column_name] = df[column_name].where(df[column_name].isnull(),
                                                                df[column_name].astype(column_type, errors='ignore'))
                    else:
                        if not date_with_time:
                            df[column_name] = pd.to_datetime(df[column_name], errors='coerce').dt.normalize()
                        else:
                            df[column_name] = pd.to_datetime(df[column_name], errors='coerce')

            # Разворачиваем длнные данные в широкие
            if not wide:
                ind = len(df.columns) - 2  # На случай, если приходит связка Ref_Key и LineNumber
                df = df.pivot(index=df.columns[:ind], columns='key', values='value')
                df.reset_index(level=list(range(ind)), inplace=True)
                df.columns.name = ''
                if ind > 1:
                    df.drop('LineNumber', axis=1, inplace=True)
    return df


def build_creation_query(df, table_name, datetime_fields):
    """Функция для создания SQL запроса на создание таблицы.
        Смотрим на тип данных столбцов и мапим их с типами в postrges
        
        datetime_fields - список полей в формате datetime64, которые нужно выгрузить в формате 
            timestamp without time zone
        """
    global pd_to_sql_mapping
    dtypes_dict = df.dtypes.to_dict()
    columns = []
    for name, dtype in dtypes_dict.items():
        if (datetime_fields is not None) and (name in datetime_fields) and (str(dtype) == 'datetime64[ns]'):
            dtype = 'timestamp without time zone'
        else:
            dtype = pd_to_sql_mapping.get(str(dtype))

        column = f'{name} {dtype}'
        columns.append(column)

    columns_str = ",\n".join(columns)
    query = f"""CREATE TABLE {table_name}(
    {columns_str})"""
    return query


def upload_table(df, table_name, creds=creds, pakage_size=10000, drop=True, datetime_fields=None):
    """Функция для вынрузки dataframe в БД через команду
    ISERT с размером пакета"""

    query_creation = build_creation_query(df, table_name, datetime_fields)
    query_drop = f"DROP TABLE IF EXISTS {table_name}"
    # При записи данных в БД заменяем np.Nan на None, чтобы в базе оказались nullы
    df = df.replace({np.nan: None})

    tuples = [tuple(x) for x in df.values.tolist()]
    conn = psycopg2.connect(creds)
    with conn.cursor() as cursor:
        try:
            if drop:
                cursor.execute(query_drop)
                cursor.execute(query_creation)
            extras.execute_values(cursor,
                                  f"INSERT INTO {table_name} VALUES %s",
                                  tuples, page_size=pakage_size)
            conn.commit()
            conn.close()
        except (Exception, psycopg2.DatabaseError) as error:
            conn.rollback()
            conn.close()
            return error


def run_query(query=None, creds=None):
    """Функция обработки запроса"""
    with closing(psycopg2.connect(creds)) as conn:
        with conn.cursor() as cursor:
            try:
                cursor.execute(query)
                conn.commit()
                conn.close()
            except (Exception, psycopg2.DatabaseError) as error:
                conn.rollback()
                conn.close()
                return error


if __name__ == '__main__':
    creds = 'dbname=codabra user=postgres password=pqghtywEVVhbLdwrU4qoHFC7bgAbLAJi host=161.35.53.116'
    db = PostgreManager(creds)

    # 6890866 - 18
    # 6935391 - 30
    # 6938116 - 27
    # 7002504 - 29
    # 7109000 - 18

    for df in db.batch_download_table('extract_amo_leads_custom',
                            filters=["l_lead_id in (6890866, 6938116, 6935391, 7002504, 7109000)"],
                            integrity_columns=['l_lead_id'],
                            batch_size=60):
        print(len(df))