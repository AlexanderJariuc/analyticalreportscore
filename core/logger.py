
import datetime
import logging

class LogManager:
	def __init__(self):
		self.logger = logging.getLogger('report_logger')
		self.logger.propagate = False
		self.logger.setLevel(logging.DEBUG)

		# console handler
		consoleHandler = logging.StreamHandler()
		consoleHandler.setLevel(logging.INFO)
		self.logger.addHandler(consoleHandler)

	def log(self, message, log_type='INFO', source=None, report=None, traceback=None):
		t_now = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S')

		if source:
			self.logger.info(f'[{t_now}][{log_type}][{source}] {str(message)}')
		else:
			self.logger.info(f'[{t_now}][{log_type}] {str(message)}')

log_manager = LogManager()
