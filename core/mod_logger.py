import traceback
from functools import wraps

from . import log_manager


def log_download(func):

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        table_name = '_'.join(func.__name__.split('_')[1:])
        try:
            result = func(self, *args, **kwargs)
            log_manager.log(f'Table {table_name} is downloaded', source=self.__module__, report=self.report)
        except Exception as e:
            log_manager.log(f'Download failed {table_name}: {e}', log_type='ERROR', source=self.__module__, report=self.report,
                            traceback=traceback.format_exc())
            raise
        return result

    return wrapper


def log_batch_download(func):

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        table_name = '_'.join(func.__name__.split('_')[2:])

        try:
            for batch, downloaded_rows, total_rows in func(self, *args, **kwargs):
                log_manager.log(f'{downloaded_rows} of {total_rows} from table {table_name} is downloaded',
                                source=self.__module__, report=self.report)
                yield batch, downloaded_rows, total_rows
        except Exception as e:
            log_manager.log(f'Download failed {table_name}: {e}', log_type='ERROR', source=self.__module__, report=self.report,
                            traceback=traceback.format_exc())
            raise

    return wrapper


def log_upload(func):

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        table_name = '_'.join(func.__name__.split('_')[1:])
        response = func(self, *args, **kwargs)

        if not response:
            log_manager.log(f'Table {table_name} is uploaded', source=self.__module__, report=self.report)
        else:
            log_manager.log(f'Table upload failed {table_name}: {response}', log_type='ERROR', source=self.__module__,
                            report=self.report,
                            traceback=traceback.format_exc())

    return wrapper


def log_build(func):

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        table_name = '_'.join(func.__name__.split('_')[1:])

        try:
            result = func(self, *args, **kwargs)
            log_manager.log(f'Table {table_name} is built', source=self.__module__, report=self.report)
        except Exception as e:
            log_manager.log(f'Table creation failed {table_name}: {e}', log_type='ERROR', source=self.__module__,
                            report=self.report,
                            traceback=traceback.format_exc())
            raise
        return result

    return wrapper


def log_run(func):

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        try:
            func(self, *args, **kwargs)
            if self.warnings:
                warning = ''.join([f'\n{warning}: {amount}' for warning, amount in self.warnings.items()])
                log_manager.log(warning, log_type='ERROR', source=self.__module__,
                                report=self.report,
                                traceback=traceback.format_exc())
        except Exception as e:
            log_manager.log(f'Crash running', log_type='ERROR', source=self.__module__,
                            report=self.report,
                            traceback=traceback.format_exc())
            raise

    return wrapper


def log_create_connector(func):

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        table_name = '_'.join(func.__name__.split('_')[1:])

        try:
            result = func(self, *args, **kwargs)
            log_manager.log(f'Connector {table_name} is created', source=self.__module__, report=self.report)
        except Exception as e:
            log_manager.log(f'Connector creation failed {table_name}: {e}', log_type='ERROR', source=self.__module__,
                            report=self.report,
                            traceback=traceback.format_exc())
            raise
        return result

    return wrapper


def log_create_custom_build(func):

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        table_name = '_'.join(func.__name__.split('_')[1:])

        try:
            result = func(self, *args, **kwargs)
            log_manager.log(f'Custom build {table_name} is created', source=self.__module__, report=self.report)
        except Exception as e:
            log_manager.log(f'Custom build creation failed {table_name}: {e}', log_type='ERROR', source=self.__module__,
                            report=self.report,
                            traceback=traceback.format_exc())
            raise
        return result

    return wrapper


def log_create_extract(func):

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        extract_group_name = '_'.join(func.__name__.split('_')[1:])

        try:
            result = func(self, *args, **kwargs)
            log_manager.log(f'Extract {extract_group_name} is created', source=self.__module__, report=self.report)
        except Exception as e:
            log_manager.log(f'Extract creation failed {extract_group_name}: {e}', log_type='ERROR', source=self.__module__,
                            report=self.report,
                            traceback=traceback.format_exc())
            raise
        return result

    return wrapper


def log_processing(func):

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        table_name = '_'.join(func.__name__.split('_')[1:])

        try:
            result = func(self, *args, **kwargs)
            log_manager.log(f'Table {table_name} is processed', source=self.__module__, report=self.report)
        except Exception as e:
            log_manager.log(f'Table creation failed {table_name}: {e}', log_type='ERROR', source=self.__module__,
                            report=self.report,
                            traceback=traceback.format_exc())
            raise
        return result

    return wrapper


def log_validation(func):

    def wrapper(self, *args, **kwargs):
        validation_name = '_'.join(func.__name__.split('_')[1:])

        try:
            messages = func(self, *args, **kwargs)
            for message in messages:
                log_manager.log(message, source=self.__module__, log_type='VALIDATION', report=self.report)
        except Exception as e:
            log_manager.log(f'Validation {validation_name} creation failed: {e}', log_type='ERROR',
                            source=self.__module__,
                            report=self.report,
                            traceback=traceback.format_exc())
            raise

    return wrapper


def log_report(cls):
    for attr in [attr for attr in dir(cls) if not attr.startswith('_')]:
        if callable(getattr(cls, attr)):
            if attr.startswith('run'):
                setattr(cls, attr, log_run(getattr(cls, attr)))
            if attr.startswith('download'):
                setattr(cls, attr, log_download(getattr(cls, attr)))
            if attr.startswith('batch_download'):
                setattr(cls, attr, log_batch_download(getattr(cls, attr)))
            if attr.startswith('upload'):
                setattr(cls, attr, log_upload(getattr(cls, attr)))
            if attr.startswith('build'):
                setattr(cls, attr, log_build(getattr(cls, attr)))
            if attr.startswith('create_connector'):
                setattr(cls, attr, log_create_connector(getattr(cls, attr)))
            if attr.startswith('create_custom_build'):
                setattr(cls, attr, log_create_custom_build(getattr(cls, attr)))
            if attr.startswith('create_extract'):
                setattr(cls, attr, log_create_extract(getattr(cls, attr)))
            if attr.startswith('processing'):
                setattr(cls, attr, log_processing(getattr(cls, attr)))
            if attr.startswith('validate'):
                setattr(cls, attr, log_validation(getattr(cls, attr)))

    return cls
